using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "ECS Asteroids/Settings")]
public class Settings : ScriptableObject
{
    
    [Range(0.1f, 2f)] public float collisionsThreshold = 1f;
    
    [Header("Player")]
    [Range(1, 3)] public int startingLives = 3;
    [Range(0.1f, 10f)] public float playerRespawnDelay = 2f;
    [Range(0.1f, 20f)] public float playerInvincibilityDelay = 6f;
    [Space(4)]
    [Range(1f, 180f)]public float rotateAngle = 10f;
    [Range(0.01f, 2f)]public float thrustStrength = 0.2f;
    [Range(0.1f, 2f)]public float maxVelocity = 1f;
    [Range(0.1f, 10f)]public float laserSpeed = 8f;
    [Range(1f, 30f)] public float laserLifetime = 4f;
    [Range(0.1f, 2f)]public float shipScale = 0.5f;
    [Range(0.1f, 2f)]public float playerLaserScale = 0.1f;
    
    [Header("Scores")]
    [Range(1, 100)] public int bigAsteroidScore = 20;
    [Range(1, 100)] public int mediumAsteroidScore = 40;
    [Range(1, 100)] public int smallAsteroidScore = 80;

    [Header("Asteroids")]
    [Range(0f, 10f)]public float randomDestinationRadius = 2f;
    [Range(1f, 30f)] public float asteroidsLifetime = 15f;
    [Space(4)]
    [Range(0.1f, 2f)]public float bigAsteroidScale = 2f;
    [Range(0, 10)]public int bigAsteroidMinNumber = 2;
    [Range(0, 10)]public int bigAsteroidMaxNumber = 4;
    [Range(0.1f, 2f)]public float bigAsteroidSpeed = 0.5f;
    [Range(0f, 10f)]public float bigAsteroidSpawnRate = 10f;
    [Space(4)]
    [Range(0.1f, 2f)]public float mediumAsteroidScale = 1f;
    [Range(0, 10)]public int mediumAsteroidMinNumber = 2;
    [Range(0, 10)]public int mediumAsteroidMaxNumber = 3;
    [Range(0.1f, 2f)]public float mediumAsteroidSpeed = 1f;
    [Range(0f, 10f)]public float mediumAsteroidSpawnRate = 6f;
    [Space(4)]
    [Range(0.1f, 2f)]public float smallAsteroidScale = 0.5f;
    [Range(0, 10)]public int smallAsteroidMinNumber = 2;
    [Range(0, 10)]public int smallAsteroidMaxNumber = 4;
    [Range(0.1f, 2f)]public float smallAsteroidSpeed = 1.5f;
    [Range(0f, 10f)]public float smallAsteroidSpawnRate = 4f;
}
