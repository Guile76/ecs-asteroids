using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class PrefabsManager : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
{
    [SerializeField] private Settings settings;
    
    [Header("Asteroids")]
    [SerializeField] private GameObject bigAsteroidPrefab;
    [SerializeField] private GameObject mediumAsteroidPrefab;
    [SerializeField] private GameObject smallAsteroidPrefab;
    // [SerializeField] private GameObject bigUfoPrefab;
    // [SerializeField] private GameObject smallUfoPrefab;

    [Header("Player")]
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private GameObject playerLaserBasePrefab;

    #region Entity Prefabs

    #region Asteroids
    
    public static Entity BigAsteroidEntityPrefabEntity;
    public static Entity MediumAsteroidEntityPrefabEntity;
    public static Entity SmallAsteroidEntityPrefabEntity;
    // public static Entity BigUfoPrefabEntity;
    // public static Entity SmallUfoPrefabEntity;

    #endregion

    #region Player

    public static Entity PlayerPrefabEntity;
    public static Entity PlayerLaserBasePrefabEntity;

    #endregion

    #endregion
    
    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        ApplyScales();
        
        referencedPrefabs.Add(bigAsteroidPrefab);
        referencedPrefabs.Add(mediumAsteroidPrefab);
        referencedPrefabs.Add(smallAsteroidPrefab);
        // referencedPrefabs.Add(bigUfoPrefab);
        // referencedPrefabs.Add(smallUfoPrefab);
        
        referencedPrefabs.Add(playerPrefab);
        referencedPrefabs.Add(playerLaserBasePrefab);
    }

    private void ApplyScales()
    {
        playerPrefab.transform.localScale = Vector3.one * settings.shipScale;
        playerLaserBasePrefab.transform.localScale = Vector3.one * settings.playerLaserScale;
        bigAsteroidPrefab.transform.localScale = Vector3.one * settings.bigAsteroidScale;
        mediumAsteroidPrefab.transform.localScale = Vector3.one * settings.mediumAsteroidScale;
        smallAsteroidPrefab.transform.localScale = Vector3.one * settings.smallAsteroidScale;
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        BigAsteroidEntityPrefabEntity = conversionSystem.GetPrimaryEntity(bigAsteroidPrefab);
        MediumAsteroidEntityPrefabEntity = conversionSystem.GetPrimaryEntity(mediumAsteroidPrefab);
        SmallAsteroidEntityPrefabEntity = conversionSystem.GetPrimaryEntity(smallAsteroidPrefab);
        // BigUfoPrefabEntity = conversionSystem.GetPrimaryEntity(bigUfoPrefab);
        // SmallUfoPrefabEntity = conversionSystem.GetPrimaryEntity(smallUfoPrefab);

        PlayerPrefabEntity = conversionSystem.GetPrimaryEntity(playerPrefab);
        PlayerLaserBasePrefabEntity = conversionSystem.GetPrimaryEntity(playerLaserBasePrefab);
    }
}
