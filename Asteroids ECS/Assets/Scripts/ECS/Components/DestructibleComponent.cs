using Unity.Entities;

public struct DestructibleComponent : IComponentData
{
    public bool IsToDestroy;
}
