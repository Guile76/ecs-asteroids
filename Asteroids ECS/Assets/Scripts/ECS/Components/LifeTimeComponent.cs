using Unity.Entities;

public struct LifeTimeComponent : IComponentData
{
    public float Value;
}
