using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class HyperspaceSystem : SystemBase
{
    private Camera _camera;

    protected override void OnUpdate()
    {
        if (!GameManager.IsGameOn)
            return;
        
        if(!GameManager.IsPlayerActive)
            return;
        
        CheckHyperspace();
    }

    private void CheckHyperspace()
    {
        if (_camera == null)
        {
            _camera = GameManager.Instance.GetMainCamera();
        }

        float halfHeight = _camera.orthographicSize;
        float halfWidth = halfHeight * _camera.aspect;

        Entities.WithAll<PlayerComponent>().ForEach((ref Translation trans) =>
        {
            if (trans.Value.z < -halfHeight)
            {
                trans.Value.z = halfHeight;
            }
            else if (trans.Value.z > halfHeight)
            {
                trans.Value.z = -halfHeight;
            }

            if (trans.Value.x < -halfWidth)
            {
                trans.Value.x = halfWidth;
            }
            else if (trans.Value.x > halfWidth)
            {
                trans.Value.x = -halfWidth;
            }
        }).ScheduleParallel();
    }
}
