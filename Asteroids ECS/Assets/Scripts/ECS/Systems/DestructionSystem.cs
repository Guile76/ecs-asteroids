using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using MRandom = Unity.Mathematics.Random;

public class DestructionSystem : SystemBase
{
    private BeginInitializationEntityCommandBufferSystem _system;
    private EntityQuery _destructibleQuery;
    private MRandom _random;

    protected override void OnCreate()
    {
        _random = new MRandom(2020);
        _system = World.GetExistingSystem<BeginInitializationEntityCommandBufferSystem>();
        _destructibleQuery = GetEntityQuery(typeof(DestructibleComponent));
    }

    protected override void OnUpdate()
    {
        if (!GameManager.IsGameOn)
            return;

        CheckDestruction();
    }

    private void CheckDestruction()
    {
        NativeArray<Entity> destructibleArray = _destructibleQuery.ToEntityArray(Allocator.Temp);

        foreach (Entity destructibleEntity in destructibleArray)
        {
            var destructibleComponent = EntityManager.GetComponentData<DestructibleComponent>(destructibleEntity);

            if (!destructibleComponent.IsToDestroy || HasComponent<PlayerComponent>(destructibleEntity))
                continue;

            if (!HasComponent<EnemyComponent>(destructibleEntity))
                continue;
            
            CheckAsteroidsDestruction(destructibleEntity);
        }

        EntityCommandBuffer ecbPackage = _system.CreateCommandBuffer();
        EntityCommandBuffer.ParallelWriter ecb = ecbPackage.AsParallelWriter();

        Entities.WithAll<DestructibleComponent>().ForEach((Entity entity, int entityInQueryIndex, in
            DestructibleComponent destructibleComponent) =>
        {
            if (!destructibleComponent.IsToDestroy || HasComponent<PlayerComponent>(entity))
                return;

            ecb.DestroyEntity(entityInQueryIndex, entity);
        }).ScheduleParallel();

        _system.AddJobHandleForProducer(Dependency);
    }

    private void CheckAsteroidsDestruction(Entity destructibleEntity)
    {
        var enemyComponent = EntityManager.GetComponentData<EnemyComponent>(destructibleEntity);
        var translationComponent = EntityManager.GetComponentData<Translation>(destructibleEntity);

        switch (enemyComponent.Type)
        {
            case EEnemy.BigAsteroid:
                EnemyData mediumAsteroidData = GameManager.Instance.GetEnemyData(EEnemy.MediumAsteroid);
                SpawnAsteroids(PrefabsManager.MediumAsteroidEntityPrefabEntity, mediumAsteroidData, _random,
                    translationComponent.Value);
                break;
            case EEnemy.MediumAsteroid:
                EnemyData smallAsteroidData = GameManager.Instance.GetEnemyData(EEnemy.SmallAsteroid);
                SpawnAsteroids(PrefabsManager.SmallAsteroidEntityPrefabEntity, smallAsteroidData, _random,
                    translationComponent.Value);
                break;
        }

        GameManager.Instance.AddToScore(enemyComponent.Type);
    }


    private void SpawnAsteroids(Entity entityPrefab, EnemyData asteroidData, MRandom random, float3 startPosition)
    {
        int spawnCount = random.NextInt(asteroidData.MinNumber, asteroidData.MaxNumber);
        var spawnsArray = new NativeArray<Entity>(spawnCount, Allocator.Temp);
        EntityManager.Instantiate(entityPrefab, spawnsArray);
        EntityManager.AddComponent(spawnsArray,typeof(MoveForwardComponent));
        EntityManager.AddComponent(spawnsArray, typeof(DestructibleComponent));
        EntityManager.AddComponent(spawnsArray, typeof(EnemyComponent));
        
        SetEntitiesType(ref spawnsArray, asteroidData, startPosition);
    }

    private void SetEntitiesType(ref NativeArray<Entity> entities, EnemyData data, float3 startPosition)
    {
        foreach (Entity entity in entities)
        {
            EntityManager.SetComponentData(entity, new Translation
            {
                Value = startPosition
            });
            EntityManager.SetComponentData(entity, new MoveForwardComponent
            {
                Speed = data.Speed,
                Direction = GetRandomDirection(startPosition)
            });
            EntityManager.SetComponentData(entity, new EnemyComponent
            {
                Type = data.Type
            });
            EntityManager.SetComponentData(entity, new DestructibleComponent
            {
                IsToDestroy = false
            });
        }
    }

    private static float3 GetRandomDirection(Vector3 position)
    {
        Vector3 randomDestination = Utils.GetRandomCirclePosition(Vector3.zero, GameManager.Instance.GetRandomDestinationRadius());
        Vector3 relativePos = randomDestination - position;
        return relativePos.normalized;
    }
}

