using Unity.Entities;
using Unity.Transforms;

public class MoveForwardSystem : SystemBase
{
    protected override void OnUpdate()
    {
        if (!GameManager.IsGameOn)
            return;

        ApplyMoveToEntities();
    }

    private void ApplyMoveToEntities()
    {
        float dt = Time.DeltaTime;

        Entities.WithAll<MoveForwardComponent>().ForEach((ref Translation trans, in MoveForwardComponent component) =>
        {
            trans.Value += component.Speed * dt * component.Direction;
        }).ScheduleParallel();
    }
}
