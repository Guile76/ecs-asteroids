using Unity.Collections;
using Unity.Entities;

public class PlayerCheckSystem : SystemBase
{
    private EntityQuery _playerQuery;

    protected override void OnCreate()
    {
        _playerQuery = GetEntityQuery(typeof(PlayerComponent));
    }

    protected override void OnUpdate()
    {
        if (!GameManager.IsGameOn)
            return;

        CheckPlayerState();
    }

    private void CheckPlayerState()
    {
        NativeArray<Entity> playerArray = _playerQuery.ToEntityArray(Allocator.Temp);

        foreach (Entity playerEntity in playerArray)
        {
            var destructibleComponent = GetComponent<DestructibleComponent>(playerEntity);
            if (!GameManager.IsPlayerActive || !destructibleComponent.IsToDestroy)
                continue;

            if (GameManager.IsPlayerInvincible)
            {
                GameManager.Instance.ConfirmInvincibility();
                continue;
            }

            GameManager.Instance.SetPlayerDestruction();
        }
    }
}
