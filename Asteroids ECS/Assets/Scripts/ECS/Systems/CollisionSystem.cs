using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class CollisionSystem : SystemBase
{
    private EntityQuery _playerQuery;
    private EntityQuery _laserQuery;
    private EntityQuery _enemyQuery;

    protected override void OnCreate()
    {
        _playerQuery = GetEntityQuery(typeof(PlayerComponent));
        _laserQuery = GetEntityQuery(typeof(LaserTag));
        _enemyQuery = GetEntityQuery(typeof(EnemyComponent));
    }

    protected override void OnUpdate()
    {
        if (!GameManager.IsGameOn)
            return;

        CheckCollisions();
    }

    private void CheckCollisions()
    {
        NativeArray<Entity> playerArray = _playerQuery.ToEntityArray(Allocator.TempJob);
        NativeArray<Entity> enemyArray = _enemyQuery.ToEntityArray(Allocator.TempJob);
        NativeArray<Entity> laserArray = _laserQuery.ToEntityArray(Allocator.TempJob);

        float collisionThreshold = GameManager.Instance.GetCollisionThreshold();

        Entities.WithAll<DestructibleComponent>().ForEach((Entity entity, ref DestructibleComponent
                destructibleComponent, in Translation translation) =>
            {
                if (HasComponent<PlayerComponent>(entity))
                {
                    foreach (Entity enemyEntity in enemyArray)
                    {
                        var enemyTranslation = GetComponent<Translation>(enemyEntity);
                        if (math.distance(translation.Value, enemyTranslation.Value) < collisionThreshold)
                        {
                            destructibleComponent.IsToDestroy = true;
                        }
                    }
                } else if (HasComponent<EnemyComponent>(entity))
                {
                    foreach (Entity laserEntity in laserArray)
                    {
                        var laserTranslation = GetComponent<Translation>(laserEntity);
                        if (math.distance(translation.Value, laserTranslation.Value) < collisionThreshold)
                        {
                            destructibleComponent.IsToDestroy = true;
                        }
                    }

                    foreach (Entity playerEntity in playerArray)
                    {
                        var playerTranslation = GetComponent<Translation>(playerEntity);
                        if (math.distance(translation.Value, playerTranslation.Value) < collisionThreshold)
                        {
                            destructibleComponent.IsToDestroy = true;
                        }
                    }
                } else if (HasComponent<LaserTag>(entity))
                {
                    foreach (Entity enemyEntity in enemyArray)
                    {
                        var enemyTranslation = GetComponent<Translation>(enemyEntity);
                        if (math.distance(translation.Value, enemyTranslation.Value) < collisionThreshold)
                        {
                            destructibleComponent.IsToDestroy = true;
                        }
                    }
                }
            }).WithReadOnly(enemyArray)
            .WithReadOnly(laserArray)
            .WithReadOnly(playerArray)
            .ScheduleParallel();

        enemyArray.Dispose(Dependency);
        laserArray.Dispose(Dependency);
        playerArray.Dispose(Dependency);
    }
}
