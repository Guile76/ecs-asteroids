using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using MRandom = Unity.Mathematics.Random;

public class AsteroidSpawnerSystem : SystemBase
{
    private float _bigAsteroidTimer;
    private float _mediumAsteroidTimer;
    private float _smallAsteroidTimer;
    private MRandom _random;
    private float _spawnRadius;
    
    protected override void OnCreate()
    {
        _random = new MRandom(2021);
        _spawnRadius = 10f;
    }

    protected override void OnUpdate()
    {
        if (!GameManager.IsGameOn)
            return;

        CheckSpawnPace();
    }

    private void CheckSpawnPace()
    {
        float dt = Time.DeltaTime;
        
        CheckBigAsteroidSpawning(dt);
        CheckMediumAsteroidSpawning(dt);
        CheckSmallAsteroidSpawning(dt);
    }

    private void CheckSmallAsteroidSpawning(float dt)
    {
        _smallAsteroidTimer -= dt;
        if (!(_smallAsteroidTimer <= 0f))
            return;

        EnemyData smallAsteroidData = GameManager.Instance.GetEnemyData(EEnemy.SmallAsteroid);
        _smallAsteroidTimer = smallAsteroidData.SpawnRate;
        SpawnAsteroids(PrefabsManager.SmallAsteroidEntityPrefabEntity, smallAsteroidData, _random, _spawnRadius);
    }

    private void CheckMediumAsteroidSpawning(float dt)
    {
        _mediumAsteroidTimer -= dt;
        if (!(_mediumAsteroidTimer <= 0f))
            return;
        
        EnemyData mediumAsteroidData = GameManager.Instance.GetEnemyData(EEnemy.MediumAsteroid);
        _mediumAsteroidTimer = mediumAsteroidData.SpawnRate;
        SpawnAsteroids(PrefabsManager.MediumAsteroidEntityPrefabEntity, mediumAsteroidData, _random, _spawnRadius);
    }

    private void CheckBigAsteroidSpawning(float dt)
    {
        _bigAsteroidTimer -= dt;
        if (!(_bigAsteroidTimer <= 0f))
            return;
        
        EnemyData bigAsteroidData = GameManager.Instance.GetEnemyData(EEnemy.BigAsteroid);
        _bigAsteroidTimer = bigAsteroidData.SpawnRate;
        SpawnAsteroids(PrefabsManager.BigAsteroidEntityPrefabEntity, bigAsteroidData, _random, _spawnRadius);
    }

    private void SpawnAsteroids(Entity prefab, EnemyData asteroidData, MRandom random, float spawnRadius)
    {
        int spawnCount = random.NextInt(asteroidData.MinNumber, asteroidData.MaxNumber);
        var spawnsArray = new NativeArray<Entity>(spawnCount, Allocator.Temp);
        EntityManager.Instantiate(prefab, spawnsArray);
        EntityManager.AddComponent(spawnsArray,typeof(MoveForwardComponent));
        EntityManager.AddComponent(spawnsArray, typeof(DestructibleComponent));
        EntityManager.AddComponent(spawnsArray, typeof(EnemyComponent));
        EntityManager.AddComponent(spawnsArray, typeof(LifeTimeComponent));
        
        SetEntitiesType(ref spawnsArray, asteroidData, spawnRadius);
    }

    private void SetEntitiesType(ref NativeArray<Entity> entities, EnemyData data, float spawnRadius)
    {
        foreach (Entity entity in entities)
        {
            float3 startPosition = Utils.GetRandomCirclePosition(Vector3.zero, spawnRadius);
            EntityManager.SetComponentData(entity, new Translation
            {
                Value = startPosition
            });
            EntityManager.SetComponentData(entity, new MoveForwardComponent
            {
                Speed = data.Speed,
                Direction = GetRandomDirection(startPosition)
            });
            EntityManager.SetComponentData(entity, new EnemyComponent
            {
                Type = data.Type
            });
            EntityManager.SetComponentData(entity, new DestructibleComponent
            {
                IsToDestroy = false
            });
            EntityManager.SetComponentData(entity, new LifeTimeComponent
            {
                Value = GameManager.Instance.GetAsteroidsLifetime()
            });
        }
    }

    private static float3 GetRandomDirection(Vector3 position)
    {
        Vector3 randomDestination = Utils.GetRandomCirclePosition(Vector3.zero, GameManager.Instance.GetRandomDestinationRadius());
        Vector3 relativePos = randomDestination - position;
        return relativePos.normalized;
    }
}
