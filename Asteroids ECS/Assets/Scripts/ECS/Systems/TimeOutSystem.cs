using Unity.Entities;

public class TimeOutSystem : SystemBase
{
    private BeginInitializationEntityCommandBufferSystem _system;
    
    protected override void OnCreate()
    {
        _system = World.GetExistingSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        EntityCommandBuffer ecbPackage = _system.CreateCommandBuffer();
        EntityCommandBuffer.ParallelWriter ecb = ecbPackage.AsParallelWriter();
        CheckEntitiesTimeOut(ecb);

        _system.AddJobHandleForProducer(Dependency);
    }

    private void CheckEntitiesTimeOut(EntityCommandBuffer.ParallelWriter ecb)
    {
        float dt = Time.DeltaTime;

        Entities.WithAll<LifeTimeComponent>().ForEach(
            (Entity entity, int entityInQueryIndex, ref LifeTimeComponent lifeTimeComponent) =>
            {
                lifeTimeComponent.Value -= dt;

                if (lifeTimeComponent.Value > 0)
                    return;

                ecb.DestroyEntity(entityInQueryIndex, entity);
            }).ScheduleParallel();
    }
}
