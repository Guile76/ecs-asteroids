using UnityEngine;
using Random = UnityEngine.Random;

public static class Utils
{
    public static Vector3 GetRandomCirclePosition(Vector3 origin, float radius)
    {
        Vector2 point = Random.insideUnitCircle.normalized * radius;

        return new Vector3(point.x, 0f, point.y) + origin;
    }
}
