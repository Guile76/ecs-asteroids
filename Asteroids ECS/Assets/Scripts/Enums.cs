public enum EEnemy
{
    BigAsteroid,
    MediumAsteroid,
    SmallAsteroid,
    Ufo,
    BigUfo
}

public enum EAudioClip
{
    Ambient,
    Shoot,
    Thrust,
    Explosion,
    Start,
    GameOver
}
