using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput), typeof(SoundManager))]
public class GameManager : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private CanvasManager canvasManager;
    [SerializeField] private Settings settings;

    public static GameManager Instance;

    public static bool IsGameOn;
    public static bool IsPlayerActive;
    public static bool IsPlayerInvincible;
    private int _playerLives;
    private int _playerScore;
    private int _bestScore;
    private float _playerRespawnDelay;
    private float _playerInvincibilityDelay;

    private PlayerInput _input;
    private SoundManager _soundManager;

    private EntityManager _entityManager;
    private Entity _player;
    private Vector3 _playerHiddenPosition;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        _input = GetComponent<PlayerInput>();
        _input.onActionTriggered += HandleAction;
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        _soundManager = GetComponent<SoundManager>();
    }

    private void Start()
    {
        SetPlayerHiddenPosition();
        InitializePlayer();
    }

    private void Update()
    {
        if (!IsGameOn)
            return;

        CheckPlayerState();
    }

    private void OnDestroy()
    {
        World.DisposeAllWorlds();
    }

    #region Game Logic
    
    private void LaunchGame()
    {
        if (IsGameOn)
            return;

        canvasManager.HideLobbyText();
        canvasManager.DisplayGameText();
        _playerLives = settings.startingLives;
        canvasManager.SetLives(_playerLives);
        canvasManager.SetScore(0);

        ResetPlayer();
        IsGameOn = true;
        IsPlayerActive = true;
        _soundManager.PlaySound(EAudioClip.Start);
    }
    
    /// <summary>
    /// Adds an amount to player score according to destroyed enemy type.
    /// </summary>
    /// <param name="type">Enemy type</param>
    public void AddToScore(EEnemy type)
    {
        switch (type)
        {
            case EEnemy.BigAsteroid:
                _playerScore += settings.bigAsteroidScore;
                break;
            case EEnemy.MediumAsteroid:
                _playerScore += settings.mediumAsteroidScore;
                break;
            case EEnemy.SmallAsteroid:
                _playerScore += settings.smallAsteroidScore;
                break;
        }
        canvasManager.SetScore(_playerScore);
    }

    /// <summary>
    /// Resets player destructible component to not to destroy.
    /// </summary>
    public void ConfirmInvincibility()
    {
        _entityManager.SetComponentData(_player, new DestructibleComponent
        {
            IsToDestroy = false
        });
    }

    #endregion
    
    #region Player Logic
    
    private void CheckPlayerState()
    {
        if (IsPlayerActive)
        {
            if (!IsPlayerInvincible)
                return;

            _playerInvincibilityDelay -= Time.deltaTime;
            if (_playerInvincibilityDelay > 0)
                return;

            IsPlayerInvincible = false;
        }
        else
        {
            _playerRespawnDelay -= Time.deltaTime;
            if (_playerRespawnDelay > 0)
                return;

            IsPlayerActive = true;
            ResetPlayer();
        }
    }

    private void SetPlayerHiddenPosition()
    {
        float z = mainCamera.orthographicSize * 3;
        _playerHiddenPosition = new Vector3(0, 0, z);
    }
    
    private void InitializePlayer()
    {
        _player = _entityManager.Instantiate(PrefabsManager.PlayerPrefabEntity);
        PlacePlayer(_playerHiddenPosition);
        _entityManager.AddComponent(_player, typeof(PhysicsVelocity));
        _entityManager.AddComponent(_player, typeof(PlayerComponent));
        _entityManager.AddComponent(_player, typeof(DestructibleComponent));
        _entityManager.SetComponentData(_player, new DestructibleComponent
        {
            IsToDestroy = false
        });
    }
    
    private void PlacePlayer(Vector3 newPosition)
    {
        _entityManager.SetComponentData(_player, new Translation
        {
            Value = newPosition
        });
    }
    
    private void ResetPlayerVelocity()
    {
        _entityManager.SetComponentData(_player, new PhysicsVelocity
        {
            Linear = float3.zero
        });
    }

    private void ResetPlayer()
    {
        ResetPlayerVelocity();
        PlacePlayer(Vector3.zero);
        IsPlayerInvincible = true;
        _playerInvincibilityDelay = settings.playerInvincibilityDelay;
    }
    
    private void Thrust()
    {
        if (!IsGameOn)
            return;
        
        if(!IsPlayerActive)
            return;

        var velocity = _entityManager.GetComponentData<PhysicsVelocity>(_player);
        Quaternion currentRotation = _entityManager.GetComponentData<Rotation>(_player).Value;
        Vector3 vectorRotation = currentRotation.normalized * Vector3.up;
        float3 newVelocity = velocity.Linear + (float3)(vectorRotation * settings.thrustStrength);

        float amplitude = Vector3.Project(newVelocity, Vector3.up).magnitude;
        if (amplitude >= settings.maxVelocity)
            return;

        _entityManager.SetComponentData(_player, new PhysicsVelocity
        {
            Linear = newVelocity
        });
        
        _soundManager.PlaySound(EAudioClip.Thrust);
    }

    private void RotatePlayerLeft()
    {
        if (!IsGameOn)
            return;
        
        if(!IsPlayerActive)
            return;

        Quaternion currentRotation = _entityManager.GetComponentData<Rotation>(_player).Value;
        Quaternion newRotation = math.mul(math.normalizesafe(currentRotation),
            Quaternion.AngleAxis(-settings.rotateAngle, Vector3.back));
        _entityManager.SetComponentData(_player, new Rotation
        {
            Value = newRotation
        });
    }

    private void RotatePlayerRight()
    {
        if (!IsGameOn)
            return;
        
        if(!IsPlayerActive)
            return;

        Quaternion currentRotation = _entityManager.GetComponentData<Rotation>(_player).Value;
        Quaternion newRotation = math.mul(math.normalizesafe(currentRotation),
            Quaternion.AngleAxis(settings.rotateAngle, Vector3.back));
        _entityManager.SetComponentData(_player, new Rotation
        {
            Value = newRotation
        });
    }
    
    /// <summary>
    /// Sets player destruction and applies logic according to remaining lives.
    /// </summary>
    public void SetPlayerDestruction()
    {
        _playerLives--;
        canvasManager.SetLives(_playerLives);
        ResetPlayerVelocity();
        PlacePlayer(_playerHiddenPosition);
        IsPlayerActive = false;

        if (_playerLives > 0)
        {
            _playerRespawnDelay = settings.playerRespawnDelay;
            _soundManager.PlaySound(EAudioClip.Explosion);
        }
        else
        {
            IsGameOn = false;
            _soundManager.PlaySound(EAudioClip.GameOver);
            if (_bestScore < _playerScore)
            {
                _bestScore = _playerScore;
            }

            _playerScore = 0;
            canvasManager.HideGameText();
            canvasManager.DisplayLobbyText();
            canvasManager.ActivateBestScore(_bestScore);
            PlacePlayer(_playerHiddenPosition);
        }
        
        _entityManager.SetComponentData(_player, new DestructibleComponent
        {
            IsToDestroy = false
        });
    }
    
    #endregion

    #region Inputs
    
    private void HandleAction(InputAction.CallbackContext context)
    {
        if (!context.performed)
            return;

        switch (context.action.name)
        {
            case "Start":
                LaunchGame();
                break;
            case "Thrust":
                Thrust();
                break;
            case "RotateLeft":
                RotatePlayerLeft();
                break;
            case "RotateRight":
                RotatePlayerRight();
                break;
            case "Shoot":
                Shoot();
                break;
        }
    }

    private float3 GetPlayerOrientation()
    {
        Quaternion currentRotation = _entityManager.GetComponentData<Rotation>(_player).Value;
        return currentRotation.normalized * Vector3.up;
    }
    
    #endregion

    #region Laser Logic
    
    private void Shoot()
    {
        if (!IsGameOn)
            return;
        
        if(!IsPlayerActive)
            return;

        Entity entity = _entityManager.Instantiate(PrefabsManager.PlayerLaserBasePrefabEntity);

        SetLaserEntityPosition(ref entity, _entityManager.GetComponentData<Translation>(_player).Value);
        SetLaserEntityMoveComponent(ref entity);
        SetLaserEntityLifeTimeComponent(ref entity);
        SetLaserEntityComponent(ref entity);
        SetLaserEntityDestructibleComponent(ref entity);
        
        _soundManager.PlaySound(EAudioClip.Shoot);
    }

    private void SetLaserEntityDestructibleComponent(ref Entity entity)
    {
        _entityManager.AddComponent(entity, typeof(DestructibleComponent));
        _entityManager.SetComponentData(entity, new DestructibleComponent
        {
            IsToDestroy = false
        });
    }

    private void SetLaserEntityComponent(ref Entity entity)
    {
        _entityManager.AddComponent(entity, typeof(LaserTag));
    }

    private void SetLaserEntityPosition(ref Entity entity, float3 position)
    {
        _entityManager.SetComponentData(entity, new Translation
        {
            Value = position
        });
    }

    private void SetLaserEntityMoveComponent(ref Entity entity)
    {
        _entityManager.AddComponent(entity, typeof(MoveForwardComponent));
        _entityManager.SetComponentData(entity, new MoveForwardComponent
        {
            Speed = settings.laserSpeed,
            Direction = GetPlayerOrientation()
        });
    }
    
    private void SetLaserEntityLifeTimeComponent(ref Entity entity)
    {
        _entityManager.AddComponent(entity, typeof(LifeTimeComponent));
        _entityManager.SetComponentData(entity, new LifeTimeComponent
        {
            Value = settings.laserLifetime
        });
    }

    #endregion

    #region Settings Utils
    
    /// <summary>
    /// Gets collision threshold.
    /// </summary>
    /// <returns>Threshold as a float.</returns>
    public float GetCollisionThreshold()
    {
        return settings.collisionsThreshold;
    }
    
    /// <summary>
    /// Gets enemy data according to its type.
    /// </summary>
    /// <param name="type">Enemy type.</param>
    /// <returns>An structure containing enemy to instantiate data.</returns>
    public EnemyData GetEnemyData(EEnemy type)
    {
        var data = new EnemyData();

        switch (type)
        {
            case EEnemy.BigAsteroid:
                data.Type = type;
                data.MinNumber = settings.bigAsteroidMinNumber;
                data.MaxNumber = settings.bigAsteroidMaxNumber;
                data.Speed = settings.bigAsteroidSpeed;
                data.SpawnRate = settings.bigAsteroidSpawnRate;
                break;
            case EEnemy.MediumAsteroid:
                data.Type = type;
                data.MinNumber = settings.mediumAsteroidMinNumber;
                data.MaxNumber = settings.mediumAsteroidMaxNumber;
                data.Speed = settings.mediumAsteroidSpeed;
                data.SpawnRate = settings.mediumAsteroidSpawnRate;
                break;
            case EEnemy.SmallAsteroid:
                data.Type = type;
                data.MinNumber = settings.smallAsteroidMinNumber;
                data.MaxNumber = settings.smallAsteroidMaxNumber;
                data.Speed = settings.smallAsteroidSpeed;
                data.SpawnRate = settings.smallAsteroidSpawnRate;
                break;
        }

        return data;
    }

    /// <summary>
    /// Gets a radius along which to randomly determine enemies destination.
    /// </summary>
    /// <returns>Radius as a float.</returns>
    public float GetRandomDestinationRadius()
    {
        return settings.randomDestinationRadius;
    }

    /// <summary>
    /// Gets asteroids lifetime.
    /// </summary>
    /// <returns>Lifetime as a float.</returns>
    public float GetAsteroidsLifetime()
    {
        return settings.asteroidsLifetime;
    }

    #endregion

    /// <summary>
    /// Gets game main camera.
    /// </summary>
    /// <returns>Camera as a UnityEngine class.</returns>
    public Camera GetMainCamera()
    {
        return mainCamera;
    }
}