using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    [SerializeField] private List<AudioData> audioData;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        PlaySound(EAudioClip.Ambient);
    }

    /// <summary>
    /// Plays a sound from the audio clips list.
    /// </summary>
    /// <param name="type">Audio clip type.</param>
    public void PlaySound(EAudioClip type)
    {
        foreach (AudioData data in audioData)
        {
            if (data.name == type)
            {
                _audioSource.PlayOneShot(data.audioClip, data.volume);
            }
        }
    }
}
