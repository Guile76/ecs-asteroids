using TMPro;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
     [SerializeField] private GameObject lobby;
     [SerializeField] private GameObject congratulationsText;
     [SerializeField] private TextMeshProUGUI bestScoreText;
     
     [SerializeField] private GameObject game;
     [SerializeField] private TextMeshProUGUI score;
     [SerializeField] private GameObject life1;
     [SerializeField] private GameObject life2;
     [SerializeField] private GameObject life3;

     private void Start()
     {
         HideGameText();
         DisplayLobbyText();
         congratulationsText.SetActive(false);
         bestScoreText.gameObject.SetActive(false);
     }

     [ContextMenu("UI/Lobby - Display")]
    public void DisplayLobbyText()
    {
        lobby.SetActive(true);
    }

    [ContextMenu("UI/Lobby - Hide")]
    public void HideLobbyText()
    {
        lobby.SetActive(false);
    }

    [ContextMenu("UI/Game - Display")]
    public void DisplayGameText()
    {
        game.SetActive(true);
    }

    [ContextMenu("UI/Game - Hide")]
    public void HideGameText()
    {
        game.SetActive(false);
    }

    public void SetLives(int value)
    {
        life1.SetActive(value > 0);
        life2.SetActive(value > 1);
        life3.SetActive(value > 2);
    }

    public void SetScore(int newScore)
    {
        score.text = newScore.ToString();
    }

    public void ActivateBestScore(int bestScore)
    {
        congratulationsText.SetActive(true);
        bestScoreText.gameObject.SetActive(true);
        bestScoreText.text = bestScore.ToString();
    }
}
