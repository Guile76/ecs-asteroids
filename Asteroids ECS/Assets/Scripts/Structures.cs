using System;
using UnityEngine;

public struct EnemyData
{
    public EEnemy Type;
    public int MinNumber;
    public int MaxNumber;
    public float Speed;
    public float SpawnRate;
}

[Serializable]
public struct AudioData
{
    public EAudioClip name;
    public AudioClip audioClip;
    [Range(0f, 1f)]public float volume;
}