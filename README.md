# ECS Asteroids

Asteroids version with ECS DOTS made in 2 days.

**Unity3D version: 2020.2.1f**

**Requirements**
- [x] Implement game with Unity DOTS
- [x] Game running on the Editor
- [x] Keyboard controls
- [x] Asteroids logic
- [x] Player ship physics/controls (used Unity Physics)
- [x] Hyperspace
- [ ] Shot variation power-up
- [X] Temporary invulnerable shield (at player respawning)
- [ ] UFO's ennemies

**Manual**

Once on the lobby, you can start the game by pressing ENTER.

Controls:
- Rotate ship left: A or left arrow.
- Rotate ship right: D or right arrow.
- Thrust: W or up arrow.
- Shoot: space bar.

Settings:

A Settings asset (Scriptable object) is available at the root of the project. Its default values are balanced for an easy to medium difficulty game.
Various values can be changed at runtime, except those regarding objects scaling.

**Architecture**

I followed a classic Unity game architecture with:
- A GameManager, in charge of the game loop, the inputs management and the player logic. Ideally, I would have split all those roles into as many managers or, I had time, as pure C# classes with dependency injection (Zenject/Extenject).
- A Sound manager (on the same object than the one above)

I used the new Input System because of its easy customizability.

As for the ECS architecture, I set a PrefabManager as a hybrid entity to ease the GameObject prefabs linking to their entities counterparts. The other entities are mainly instantiated with entity prefabs.
I created several systems (and their related components) to manage the core game logics.

I chose an offensive programming approach, both because of the short delay but also to be sure to handle every possible bug related to ECS.

**Known bugs**
- Collision threshold: there is only one generic threshold that leads to collision being not always precise.

**Encountered challenges**
- My ECS knowledge was a little bit outdated; last professional protoyping with it was almost 2 years ago. 
- I wasted some time with Quaternion and Vector issues, that were at last simply solved.
- I spent some time to check if the ECS features I was planning to use were not about to be deprecated. I plan to continue working on this project as a personnal one.

**Benefits**
- Good ECS update.
- R&D-like process. I've had time to investigate on several ECS related subject for which I didn't find any moment before, like ECS Unity Physics.

**Scheduled improvements**
- Visual effects: visual tips are missing (invincibility, thrust, explosions...). I would like to manipulate Shaders into an ECS project.
- Base game missing features. The UFO's will be easy to implement as their logic is almost identical to the Asteroids management (spawning, moving, collision and destruction systems).
